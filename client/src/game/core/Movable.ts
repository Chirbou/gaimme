import { Directions } from 'src/game/constants/Directions';

export interface Movable {
  move(directions: Directions[]): void
}
