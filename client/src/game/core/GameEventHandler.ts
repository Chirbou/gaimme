import { NetworkEventHandler } from 'src/engine/NetworkEventHandler'
import { ClientEvent } from '@common/ClientEvent'
import { UtilRandom } from '@common/UtilRandom'
import { Game } from '../Game'
import { Directions } from 'src/game/constants/Directions'
import { InputHandler } from 'src/engine/InputHandler'
import { InputKeys } from 'src/engine/InputKeys'
import { CommonConfig } from '@common/CommonConfig'
import { PowerUpTakenNetwork } from '@common/PowerUpTakenNetwork'
import { PowerUpNetwork } from '@common/PowerUpNetwork'
import { PowerUpTypes } from '@common/constants/PowerUpTypes'
import { SpeedBoost } from '../SpeedBoost'
import { Coords } from '@common/Coords'
import { SizeBoost } from '../SizeBoost'
import { InitData } from '@common/InitData'
import { PlayerNetwork } from '@common/PlayerNetwork'
import { PositionedItem } from '@common/PositionedItem'
import { RemotePlayer } from '../RemotePlayer'
import { Collider } from '@common/Collider'

export class GameEventHandler {
  static checkPowerUps() {
    const mainPlayer = Game.getHumanPlayer()
    for (const powerUp of Game.powerUps) {
      if (powerUp.isTaken(mainPlayer)) {
        powerUp.triggerEffect(mainPlayer)
        const networkPowerUp: PowerUpTakenNetwork = {
          id: powerUp.id,
          position: powerUp.position,
          type: powerUp.type,
          playerId: mainPlayer.id,
        }
        NetworkEventHandler.emit(ClientEvent.POWER_UP_TAKEN, networkPowerUp)
      }
    }
  }

  static checkPlayerBump() {
    const mainPlayer = Game.getHumanPlayer()
    for (const remotePlayer of Game.remotePlayers) {
      if (
        Collider.areColliding(
          mainPlayer.position,
          remotePlayer.position,
          mainPlayer.size,
          remotePlayer.size
        )
      ) {
        mainPlayer.bump(remotePlayer.velocity, remotePlayer.size)
        remotePlayer.bump(mainPlayer.velocity, mainPlayer.size)
      }
    }
  }

  static setMainPlayerCustomData(name: string, color: string) {
    const mainPlayer = Game.getHumanPlayer()
    if (mainPlayer) {
      name = name || mainPlayer.id
      mainPlayer.name = name
      mainPlayer.color = color
      NetworkEventHandler.emit(ClientEvent.SEND_PLAYER_CUSTOM_DATA, {
        name,
        color,
      })
    }
  }

  static handleSetPlayers(players: PlayerNetwork[]) {
    Game.updateRemotePlayers(players)
  }

  static handleInit(initData: InitData) {
    Game.createHumanPlayer(initData.playerId, initData.color, initData.position)
    GameEventHandler.createRemotePlayers(initData.players)
    const input = document.getElementById('player-color') as HTMLInputElement
    input.value = UtilRandom.rgbaToHex(initData.color).slice(0, 7)
    Game.run()
  }

  static handlePowerUps(powerUps: PowerUpNetwork[]) {
    for (const powerUp of powerUps) {
      GameEventHandler.handlePowerUp(powerUp)
    }
  }

  static handlePowerUp(powerUp: PowerUpNetwork) {
    switch (powerUp.type) {
      case PowerUpTypes.SPEED_BOOST:
        Game.addPowerUp(
          new SpeedBoost(
            powerUp.id,
            {
              x: powerUp.position.x,
              y: powerUp.position.y,
            } as Coords,
            powerUp.power
          )
        )
        break
      case PowerUpTypes.SIZE_BOOST:
        Game.addPowerUp(
          new SizeBoost(
            powerUp.id,
            {
              x: powerUp.position.x,
              y: powerUp.position.y,
            } as Coords,
            powerUp.power
          )
        )
        break
    }
  }

  static handleMove() {
    const mainPlayer = Game.getHumanPlayer()

    for (const player of Game.players) {
      player.updatePosition()
    }

    const directions = []

    if (InputHandler.keyState.get(InputKeys.UP)) {
      directions.push(Directions.UP)
    }
    if (InputHandler.keyState.get(InputKeys.LEFT)) {
      directions.push(Directions.LEFT)
    }
    if (InputHandler.keyState.get(InputKeys.RIGHT)) {
      directions.push(Directions.RIGHT)
    }
    if (InputHandler.keyState.get(InputKeys.DOWN)) {
      directions.push(Directions.DOWN)
    }

    mainPlayer.move(directions)
  }

  static createRemotePlayers(players: PositionedItem[]) {
    for (const player of players) {
      if (player.id !== Game.humanPlayer.id) {
        Game.remotePlayers.push(new RemotePlayer(player.id, player.position))
      }
    }
  }

  static createRemotePlayer(player: PositionedItem) {
    if (!Game.humanPlayer.id) {
      console.error('Possible async problem (Join received before Init)')
      return
    }
    if (player.id === Game.humanPlayer.id) return
    Game.remotePlayers.push(new RemotePlayer(player.id, player.position))
  }

  static removeRemotePlayer(id: string) {
    const removedPlayer = Game.remotePlayers.find((player) => player.id === id)
    if (removedPlayer) {
      removedPlayer.destroy()
      Game.remotePlayers = Game.remotePlayers.filter(
        (player) => player.id !== id
      )
    } else {
      console.error(`Wanted to remove player ${id} but could not find it.`)
    }
  }

  static removePowerUp(id: string) {
    const removedPowerUp = Game.powerUps.find((powerUp) => powerUp.id === id)
    if (removedPowerUp) {
      removedPowerUp.destroy()
      Game.powerUps = Game.powerUps.filter((powerUp) => powerUp.id !== id)
    } else {
      console.error(`Wanted to remove power up ${id} but could not find it.`)
    }
  }

  static sendPlayerData() {
    NetworkEventHandler.emit(
      ClientEvent.SEND_PLAYER_DATA,
      Game.getHumanPlayer().playerNetwork
    )
  }

  static checkOffLimits() {
    const mainPlayer = Game.getHumanPlayer()

    if (mainPlayer.position.x < 0) {
      mainPlayer.position.x =
        CommonConfig.GAME_WIDTH + mainPlayer.position.x - mainPlayer.size.width
    } else if (
      mainPlayer.position.x + mainPlayer.size.width >
      CommonConfig.GAME_WIDTH
    ) {
      mainPlayer.position.x =
        (mainPlayer.position.x + mainPlayer.size.width) %
        CommonConfig.GAME_WIDTH
    } else if (mainPlayer.position.y < 0) {
      mainPlayer.position.y =
        CommonConfig.GAME_HEIGHT +
        mainPlayer.position.y -
        mainPlayer.size.height
    } else if (
      mainPlayer.position.y + mainPlayer.size.height >
      CommonConfig.GAME_HEIGHT
    ) {
      mainPlayer.position.y =
        (mainPlayer.position.y + mainPlayer.size.height) %
        CommonConfig.GAME_HEIGHT
    }
  }
}
