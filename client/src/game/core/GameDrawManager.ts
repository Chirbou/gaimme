import { DOMConstants } from 'src/engine/constants/DOMConstants';
import { DrawableGameObject } from './DrawableGameObject';
import { SpecificDrawManager } from '../../engine/SpecificDrawManager';
import { CommonConfig } from '@common/CommonConfig';

export class GameDrawManager extends SpecificDrawManager {
  private backgroundQueue: DrawableGameObject[] = [];
  private foregroundQueue: DrawableGameObject[] = [];

  constructor() {
    super(DOMConstants.GAME_CANVAS_ID, true);
  }

  addToQueue(element: DrawableGameObject, foreground = false) {
    if (!foreground) {
      this.backgroundQueue.push(element);
    }

    else {
      this.foregroundQueue.push(element);
    }
  }

  removeFromQueue(id: string) {
    this.backgroundQueue = this.backgroundQueue.filter(el => el.id !== id);
  }

  draw(): void {
    this.handleRefresh(this.drawGame);
  }

  drawGame = () => {
    this.ctx.clearRect(0, 0, CommonConfig.GAME_WIDTH, CommonConfig.GAME_HEIGHT)
    for (const element of this.backgroundQueue) {
      element.handleDraw();
    }

    for (const element of this.foregroundQueue) {
      element.handleDraw();
    }
  }
}

export const gameDrawManager = new GameDrawManager();
