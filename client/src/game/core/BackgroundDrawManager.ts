import { GameColors } from 'src/game/constants/GameColors'
import { EngineConstants } from 'src/engine/constants/EngineConstants'
import { SpecificDrawManager } from '../../engine/SpecificDrawManager'
import { DOMConstants } from 'src/engine/constants/DOMConstants'
import { CommonConfig } from '@common/CommonConfig'

export class BackgroundDrawManager extends SpecificDrawManager {
  refreshRate = +EngineConstants.RENDER_ONCE

  constructor() {
    super(DOMConstants.BG_CANVAS_ID)
  }

  draw(): void {
    this.handleRefresh(this.drawBackground)
  }

  drawBackground = () => {
    // const bg = document.getElementById('bg') as HTMLImageElement;
    this.ctx.fillStyle = GameColors.BLACK
    // TODO : fix bug avec du onload sur l'image

    const img = new Image()
    img.onload = () => {
      this.ctx.drawImage(
        img,
        0,
        0,
        CommonConfig.GAME_WIDTH,
        CommonConfig.GAME_HEIGHT
      )
    }
    img.src = './images/bg-forest.jpg'
  }
}

export const backgroundDrawManager = new BackgroundDrawManager()
