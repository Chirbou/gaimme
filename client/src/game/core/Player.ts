import { Coords } from '@common/Coords'
import { CommonConfig } from '@common/CommonConfig'
import { DrawableGameObject } from './DrawableGameObject'
import { PlayerNetwork } from '@common/PlayerNetwork'
import { GameConstants } from '../constants/GameConstants'
import { Vector2D } from '@common/Vector2D'
import { Size } from '@common/Size'

export abstract class Player extends DrawableGameObject {
  constructor(
    id: string,
    position: Coords,
    color: string,
    foreground: boolean
  ) {
    super(
      position,
      { width: CommonConfig.PLAYER_WIDTH, height: CommonConfig.PLAYER_HEIGHT },
      id,
      foreground
    )
    this.currentMaxVelocity = +GameConstants.INITIAL_MAX_PLAYER_VELOCITY
    this.currentMaxAcceleration = +GameConstants.INITIAL_MAX_PLAYER_ACCELERATION
    this.score = 0
    this.name = id
    this.color = color
  }

  public currentMaxAcceleration: number
  public currentMaxVelocity: number

  public acceleration: Vector2D = {
    x: +GameConstants.INITIAL_PLAYER_ACCELERATION,
    y: +GameConstants.INITIAL_PLAYER_ACCELERATION,
  }

  public velocity: Vector2D = {
    x: +GameConstants.INITIAL_PLAYER_VELOCITY,
    y: +GameConstants.INITIAL_PLAYER_VELOCITY,
  }

  name: string
  score: number
  color: string
  friction: number = GameConstants.INITIAL_PLAYER_FRICTION

  get playerNetwork(): PlayerNetwork {
    return {
      position: this.position,
      size: this.size,
      score: this.score,
      id: this.id,
      color: this.color,
      velocity: this.velocity,
      name: this.name,
    }
  }

  get center(): Coords {
    return {
      x: this.position.x + this.size.width / 2,
      y: this.position.y + this.size.height / 2,
    }
  }

  bump(velocity: Vector2D, size: Size): void {
    const remoteWeight =
      Math.sqrt(size.width * size.height) / GameConstants.GAME_LOOP_REFRESH_RATE
    const deltaX = velocity.x - this.velocity.x
    const deltaY = velocity.y - this.velocity.y

    this.velocity.x = deltaX * remoteWeight
    this.velocity.y = deltaY * remoteWeight

    // this.velocity.x = -this.velocity.x
    // this.velocity.y = -this.velocity.y
    this.position.x +=
      ((size.width + Math.abs(velocity.x)) / 4) * Math.sign(this.velocity.x)
    this.position.y +=
      ((size.height + Math.abs(velocity.y)) / 4) * Math.sign(this.velocity.y)
  }

  updatePosition(): void {
    this.velocity.x += this.acceleration.x
    this.velocity.y += this.acceleration.y

    if (this.velocity.x > this.currentMaxVelocity) {
      this.velocity.x = this.currentMaxVelocity
    } else if (this.velocity.x < -this.currentMaxVelocity) {
      this.velocity.x = -this.currentMaxVelocity
    }
    if (this.velocity.y > this.currentMaxVelocity) {
      this.velocity.y = this.currentMaxVelocity
    } else if (this.velocity.y < -this.currentMaxVelocity) {
      this.velocity.y = -this.currentMaxVelocity
    }

    this.computeFrictionSimple()

    this.position.x += this.velocity.x
    this.position.y += this.velocity.y
  }

  computeFriction(): void {
    const magnitude = Math.sqrt(
      this.position.x * this.position.x + this.position.y * this.position.y
    )
    const direction = Math.atan2(this.position.y, this.position.x)
    const x = Math.cos(direction) * magnitude
    const y = Math.sin(direction) * magnitude
    let speed = Math.sqrt(
      this.velocity.x * this.velocity.x + this.velocity.y * this.velocity.y
    )
    const angle = Math.atan2(this.velocity.y, this.velocity.x)
    if (speed > this.friction) {
      speed -= this.friction
    } else {
      speed = 0
    }
    this.velocity.x = Math.cos(angle) * speed
    this.velocity.y = Math.sin(angle) * speed
  }

  computeFrictionSimple(): void {
    this.velocity.x = this.velocity.x * GameConstants.FRICTION
    this.velocity.y = this.velocity.y * GameConstants.FRICTION
  }
}
