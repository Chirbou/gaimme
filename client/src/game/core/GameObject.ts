import { Coords } from "@common/Coords";
import { Size } from "@common/Size";

export abstract class GameObject {
  id: string
  position: Coords
  size: Size

  constructor(position: Coords, size: Size, id: string) {
    this.position = position
    this.size = size
    this.id = id
  }
}
