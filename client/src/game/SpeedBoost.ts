import { Coords } from '@common/Coords'
import { GameConstants } from 'src/game/constants/GameConstants'
import { GameColors } from 'src/game/constants/GameColors'
import { gameDrawManager } from './core/GameDrawManager'
import { PowerUp } from './core/PowerUp'
import { PowerUpTypes } from '@common/constants/PowerUpTypes'
import { Alterations } from './constants/Alterations'
import { BlinkAnimation } from './animation/BlinkAnimation'
import { HumanPlayer } from './HumanPlayer'
import { SpinningAnimation } from './animation/SpinningAnimation'
import { Player } from './core/Player'

export class SpeedBoost extends PowerUp {
  color: string = ''
  type = PowerUpTypes.SPEED_BOOST
  animations = [new SpinningAnimation(this.power)]

  constructor(id: string, position: Coords, power: number) {
    super(
      id,
      position,
      {
        width: GameConstants.SPEED_BOOST_WIDTH * power,
        height: GameConstants.SPEED_BOOST_HEIGHT * power,
      },
      power
    )
  }

  draw() {
    if (!this.exists) return
    if (!this.position) {
      console.error('No position in drawSpeedBoost')
      return
    }
    gameDrawManager.ctx.fillStyle = GameColors.SPEED_BOOST
    gameDrawManager.drawRect(this.position, this.size)
  }

  handleEffect(player: HumanPlayer) {
    // // Animation
    // const blinkAnimation = player.animations.find(a => a instanceof BlinkAnimation) as BlinkAnimation;
    // if (blinkAnimation) {
    //   blinkAnimation.increaseBlinkRate();
    // } else {
    //   player.animations.push(new BlinkAnimation([GameColors.SPED_UP_PLAYER_1, GameColors.SPED_UP_PLAYER_2]));
    // }

    player.alteration = Alterations.SPED_UP

    // Velocity bonus
    const speedBonus = +GameConstants.SPEED_BOOST_VELOCITY_BONUS * this.power
    let delta = speedBonus
    let triggerCallback = true

    if (
      player.currentMaxVelocity + speedBonus >=
      GameConstants.MAX_PLAYER_VELOCITY
    ) {
      delta = GameConstants.MAX_PLAYER_VELOCITY - player.currentMaxVelocity
      player.currentMaxVelocity = GameConstants.MAX_PLAYER_VELOCITY
    } else if (
      player.currentMaxVelocity === GameConstants.MAX_PLAYER_VELOCITY
    ) {
      triggerCallback = false
    } else {
      player.currentMaxVelocity += speedBonus
    }

    // Velocity bonus end
    triggerCallback &&
      setTimeout(() => {
        player.currentMaxVelocity -= delta
        if (!player.isSpedUp) {
          player.alteration = Alterations.NO_ALTERATION
          player.animations = player.animations.filter(
            (a) => !(a instanceof BlinkAnimation)
          )
        }
        // else if (blinkAnimation) {
        //   blinkAnimation.decreaseBlinkRate()
        // }
      }, GameConstants.SPEED_BOOST_EFFECT_DURATION)
  }
}
