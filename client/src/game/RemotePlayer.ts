import { Coords } from '@common/Coords'
import { UtilRandom } from '@common/UtilRandom'
import { gameDrawManager as drawManager } from './core/GameDrawManager'
import { Player } from './core/Player'
import { GUIConstants } from './gui/GUIConstants'

export class RemotePlayer extends Player {
  constructor(id: string, position: Coords) {
    super(id, position, '#AAAAAA', false)
  }

  draw() {
    if (UtilRandom.isLight(this.color)) {
      drawManager.ctx.strokeStyle = 'black'
      drawManager.ctx.lineWidth = 2
    } else {
      drawManager.ctx.strokeStyle = '#CCCCCC'
      drawManager.ctx.lineWidth = 2
    }

    const textPosition: Coords = {
      x: this.center.x,
      y: this.position.y - GUIConstants.NAME_OFFSET,
    }
    const text = this.name.slice(0, 25)
    drawManager.ctx.font = '8px Trebuchet MS'
    drawManager.strokeText(textPosition, text)
    drawManager.drawText(textPosition, text, 0.8)
    drawManager.ctx.lineWidth = 1
    drawManager.drawRect(this.position, this.size)
  }
}
