import { Coords } from '@common/Coords'
import { GameColors } from 'src/game/constants/GameColors'
import { gameDrawManager } from './core/GameDrawManager'
import { PowerUp } from './core/PowerUp'
import { PowerUpTypes } from '@common/constants/PowerUpTypes'
import { CommonConfig } from '@common/CommonConfig'
import { GameConstants } from './constants/GameConstants'
import { HumanPlayer } from './HumanPlayer'
import { BlinkAnimation } from './animation/BlinkAnimation'

export class SizeBoost extends PowerUp {
  color: string = ''
  type = PowerUpTypes.SIZE_BOOST
  animations = [
    new BlinkAnimation(
      [GameColors.POWER_UP_BLINK_1, GameColors.POWER_UP_BLINK_2],
      this.power
    ),
  ]

  constructor(id: string, position: Coords, power: number) {
    super(
      id,
      position,
      {
        width: GameConstants.SPEED_BOOST_WIDTH * power,
        height: GameConstants.SPEED_BOOST_HEIGHT * power,
      },
      power
    )
  }

  draw() {
    if (!this.exists) return
    if (!this.position) {
      console.error('No position in drawSpeedBoost,cow-boy !')
      return
    }
    gameDrawManager.drawRect(this.position, this.size)
  }

  handleEffect(player: HumanPlayer) {
    player.size.width += (CommonConfig.PLAYER_WIDTH * this.power) / 2
    player.size.height += (CommonConfig.PLAYER_HEIGHT * this.power) / 2

    setTimeout(() => {
      player.size.width -= (CommonConfig.PLAYER_WIDTH * this.power) / 2
      player.size.height -= (CommonConfig.PLAYER_HEIGHT * this.power) / 2
    }, GameConstants.SPEED_BOOST_EFFECT_DURATION)
  }
}
