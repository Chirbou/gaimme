export enum GameColors {
  WHITE = "#FFFFFF",
  BLACK = "#000000",
  MAIN_PLAYER = '#FF0000',
  REMOTE_PLAYER = '#FFFFFF',
  SPED_UP_PLAYER_1 = '#FF9922',
  SPED_UP_PLAYER_2 = '#997733',
  POWER_UP_BLINK_1 = "#123456",
  POWER_UP_BLINK_2 = "#62F4C6",
  SPEED_METER_ARROW = '#FF0000',
  SPEED_METER_MAX_FILL = "0000CC",
  SPEED_BOOST = "#FF8822",
  ACCELERATION_METER_ARROW = '#17EC3C',
  SIZE_BOOST = "#0000FF",
}
