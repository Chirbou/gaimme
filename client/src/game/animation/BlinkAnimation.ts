import { GameColors } from 'src/game/constants/GameColors'
import { GameConstants } from 'src/game/constants/GameConstants'
import { Animation } from '../core/Animation'
import { gameDrawManager } from '../core/GameDrawManager'

const REFRESH_PER_SEC = 1000 / +GameConstants.GAME_LOOP_REFRESH_RATE

export class BlinkAnimation extends Animation {
  private MIN_BLINK_RATE = REFRESH_PER_SEC
  private MAX_BLINK_RATE = 1
  private BLINK_OFFSET = 5

  private blinkRate = this.MIN_BLINK_RATE
  private blinked = false
  private _counter = 0
  public colors: GameColors[]

  get blinkSpeed(): number {
    return this.blinkRate / this.speed
  }

  constructor(colors: GameColors[], speed: number = 1) {
    super(speed)
    if (colors.length < 2) {
      console.error('Cannot create BlinkAnimation with less than 2 colors.')
    }
    this.colors = colors
  }

  increaseBlinkRate() {
    if (this.blinkRate > this.MAX_BLINK_RATE - this.BLINK_OFFSET) {
      this.blinkRate -= this.BLINK_OFFSET
    } else {
      this.blinkRate = this.MAX_BLINK_RATE
    }
  }

  decreaseBlinkRate() {
    if (this.blinkRate < this.MIN_BLINK_RATE + this.BLINK_OFFSET) {
      this.blinkRate += this.BLINK_OFFSET
    } else {
      this.blinkRate += this.MIN_BLINK_RATE
    }
  }

  animate(): void {
    if (!this.enabled) return
    if (this.blinked) {
      gameDrawManager.ctx.fillStyle = this.colors[0]
      if (this._counter >= this.blinkSpeed) {
        this._counter = 0
        this.blinked = false
      }
    } else {
      gameDrawManager.ctx.fillStyle = this.colors[1]
      if (this._counter >= this.blinkSpeed) {
        this._counter = 0
        this.blinked = true
      }
    }
    this._counter++
  }
}
