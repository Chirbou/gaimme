import { Coords } from '@common/Coords'
import { DOMConstants } from 'src/engine/constants/DOMConstants'
import { EngineConstants } from 'src/engine/constants/EngineConstants'
import { GameConfig } from 'src/game/config/GameConfig'
import { CommonConfig } from '@common/CommonConfig'
import { Size } from '@common/Size'

export abstract class SpecificDrawManager {
  ctx: CanvasRenderingContext2D
  canvas: HTMLCanvasElement
  refreshRate: number = EngineConstants.RENDER_ONCE_PER_CYCLE
  private counterUntilRefresh: number = 1
  private wasInit = false

  constructor(canvasId: DOMConstants, enableAlpha = false) {
    this.canvas = document.getElementById(canvasId) as HTMLCanvasElement
    this.ctx = this.canvas.getContext('2d', {
      alpha: enableAlpha,
    }) as CanvasRenderingContext2D
  }

  abstract draw(): void

  scaleToWindow(
    canvasWidth: number,
    canvasHeight: number,
    gameToWindowRatioWidth: number,
    gameToWindowRatioHeight: number
  ) {
    this.canvas.width = canvasWidth
    this.canvas.height = canvasHeight
    this.ctx.scale(gameToWindowRatioWidth, gameToWindowRatioHeight)
    this.wasInit = false
  }

  handleRefresh(draw: () => void) {
    if (this.refreshRate === EngineConstants.RENDER_ONCE) {
      if (this.wasInit) return
      draw()
      this.wasInit = true
    }
    if (this.counterUntilRefresh < this.refreshRate) {
      this.counterUntilRefresh += 1
    } else {
      draw()
      this.counterUntilRefresh = 1
    }
  }

  drawRect(position: Coords, size: Size, alpha = 1) {
    this._handlePlayerOverflowSplit(position, size)

    this.ctx.beginPath()
    this.ctx.globalAlpha = alpha
    this.ctx.fillRect(position.x, position.y, size.width, size.height)
    this.ctx.globalAlpha = 1
  }

  strokeRect(position: Coords, size: Size, alpha = 1) {
    this._handlePlayerOverflowSplit(position, size)

    this.ctx.beginPath()
    this.ctx.globalAlpha = alpha
    this.ctx.strokeRect(position.x, position.y, size.width, size.height)
    this.ctx.globalAlpha = 1
  }

  drawArrow(p1: Coords, p2: Coords, alpha = 1) {
    this.ctx.beginPath()
    this.ctx.moveTo(p1.x, p1.y)
    this.ctx.lineTo(p2.x, p2.y)
    this.ctx.stroke()
  }

  drawText(position: Coords, text: string, alpha = 1) {
    this.ctx.beginPath()
    this.ctx.globalAlpha = alpha
    const textWidth = this.ctx.measureText(text).width
    this.ctx.fillText(text, position.x - textWidth / 2, position.y)
    this.ctx.stroke()
    this.ctx.globalAlpha = 1
  }

  strokeText(position: Coords, text: string, alpha = 1) {
    this.ctx.beginPath()
    this.ctx.globalAlpha = alpha
    const textWidth = this.ctx.measureText(text).width
    this.ctx.strokeText(text, position.x - textWidth / 2, position.y)
    this.ctx.stroke()
    this.ctx.globalAlpha = 1
  }

  _handlePlayerOverflowSplit(position: Coords, size: Size) {
    if (GameConfig.ENABLE_SPLIT_PLAYER_WHEN_OVERFLOW) {
      const isOverflowX = position.x + size.width > CommonConfig.GAME_WIDTH
      const isOverflowY = position.y + size.height > CommonConfig.GAME_HEIGHT
      if (isOverflowX) {
        const overflowPosition: Coords = {
          x: position.x - CommonConfig.GAME_WIDTH,
          y: position.y,
        }
        this.ctx.fillRect(
          overflowPosition.x,
          overflowPosition.y,
          size.width,
          size.height
        )
      }
      if (isOverflowY) {
        const overflowPosition: Coords = {
          x: position.x,
          y: position.y - CommonConfig.GAME_HEIGHT,
        }
        this.ctx.fillRect(
          overflowPosition.x,
          overflowPosition.y,
          size.width,
          size.height
        )
      }
    }
  }
}
