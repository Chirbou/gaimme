export enum EngineConstants {
  RENDER_ONCE = -1,
  RENDER_ONCE_PER_CYCLE = 1,
  FPS_30 = 33.333,
  FPS_60 = 16.667,
}
