import { Socket } from 'socket.io-client'
import { ClientEvent } from '@common/ClientEvent'
import { ServerEvent } from '@common/ServerEvent'
import { UtilParse } from '@common/UtilParse'
import { InitData } from '@common/InitData';
import { PositionedItem } from '@common/PositionedItem';
import { PowerUpNetwork } from '@common/PowerUpNetwork';
import { GameEventHandler } from 'src/game/core/GameEventHandler'
import { PlayerNetwork } from '@common/PlayerNetwork'

export class NetworkEventHandler {
  private static socket: typeof Socket
  private static HAS_BEEN_INIT = false;
  static init(socket: typeof Socket) {
    NetworkEventHandler.socket = socket
    NetworkEventHandler.listenEvents();
  }

  static listenEvents() {
    NetworkEventHandler.on(ServerEvent.SET_PLAYERS, (positionsStr: string) => {
      NetworkEventHandler._successOrAbort(() => {
        let players = UtilParse.parse(positionsStr) as PlayerNetwork[];
        GameEventHandler.handleSetPlayers(players);
      })
    })


    NetworkEventHandler.on(ServerEvent.SERVER_INIT, () => {
      window.location.reload();
    })

    NetworkEventHandler.on(ServerEvent.INIT, (initDataStr: string) => {
      if (!NetworkEventHandler.HAS_BEEN_INIT) {
        NetworkEventHandler.HAS_BEEN_INIT = true
        const initData = UtilParse.parse(initDataStr) as InitData
        GameEventHandler.handleInit(initData)
      } else {
        window.location.reload();
      }
    })

    NetworkEventHandler.on(ServerEvent.EMIT_POWERUPS, (powerUpsStr: string) => {
      const powerUps = UtilParse.parse(powerUpsStr) as PowerUpNetwork[]
      GameEventHandler.handlePowerUps(powerUps)
    })

    NetworkEventHandler.on(ServerEvent.PLAYER_JOIN, (positionedItemStr: string) => {
      const positionedItem = UtilParse.parse(positionedItemStr) as PositionedItem;
      GameEventHandler.createRemotePlayer(positionedItem)
    })

    NetworkEventHandler.on(ServerEvent.PLAYER_LEAVE, (idStr: string) => {
      NetworkEventHandler._successOrAbort(() => {
        GameEventHandler.removeRemotePlayer(UtilParse.parse(idStr))
      });
    })

    NetworkEventHandler.on(ServerEvent.POWER_UP_ADDED, (powerUpStr: string) => {
      const powerUp = UtilParse.parse(powerUpStr) as PowerUpNetwork;
      GameEventHandler.handlePowerUp(powerUp)
    })

    NetworkEventHandler.on(ServerEvent.POWER_UP_REMOVED, (idStr: string) => {
      NetworkEventHandler._successOrAbort(() => {
        GameEventHandler.removePowerUp(UtilParse.parse(idStr))
      });
    })
  }

  static emit(eventName: ClientEvent, data: any = null) {
    NetworkEventHandler.socket.emit(eventName, UtilParse.stringify(data));
  }

  static on(eventName: ServerEvent, callback: any) {
    NetworkEventHandler.socket.on(eventName, callback);
  }

  private static _successOrAbort(f: () => void) {
    try {
      f();
    } catch (e) {
      console.warn("Aborted event")
    }
  }


}
