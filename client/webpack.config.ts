import path from 'path'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import { Configuration } from 'webpack'
// import CopyPlugin from 'copy-webpack-plugin';

const config: Configuration = {
  entry: path.resolve(__dirname, './src/index.ts'),
  output: {
    filename: 'main.[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
  },
  mode: process.env.NODE_ENV === 'prod' ? 'production' : 'development',
  watch: process.env.NODE_ENV === 'prod' ? false : true,
  target: `web`,
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css?$/,
        use: ['style-loader', 'css-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.jpe?g$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'images',
        },
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-typescript'],
          },
        },
      },
    ],
  },
  devtool: 'source-map',
  plugins: [
    // new CopyPlugin({
    //   patterns: [
    //     {
    //       from: './bg-forest.jpg',
    //       to: './bg-forest.jpg',
    //     },
    //   ]
    // }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/index.html'),
    }),
  ],
  resolve: {
    plugins: [new TsconfigPathsPlugin({})],
    extensions: [`.css`, `.js`, `.ts`],
  },
}

if (process.env.NODE_ENV === 'prod') {
  config.plugins?.unshift(new CleanWebpackPlugin())
}

export default config
