export enum AppConfig {
  PING_DELAY = 50,
  PORT = 80,
  HOST = 'localhost',
}
