import { PowerUpTakenNetwork } from '@common/PowerUpTakenNetwork'
import { ServerEvent } from '@common/ServerEvent'
import { EventHandler } from 'src/EventHandler'
import { Game } from './Game'
import { PowerUp } from './PowerUp'
import { PowerUpTypes } from '@common/constants/PowerUpTypes'

export class GameEventHandler {
  static handlePowerUpTaken(powerUp: PowerUpTakenNetwork) {
    if (!powerUp) {
      console.log(`User sent bad data when eating his power up`)
      return
    }
    if (!Game.powerUps.find((p) => p.id === powerUp.id)) {
      console.log(`User tried to eat an already eaten power up`)
      return
    }

    const player = Game.getPlayer(powerUp.playerId)
    if (player) {
      player.score = GameEventHandler._incrementScore(player.score)
    }

    Game.removePowerUp(powerUp.id)
    EventHandler.broadcast(ServerEvent.POWER_UP_REMOVED, powerUp.id)

    let newPowerUp: PowerUp

    switch (powerUp.type) {
      case PowerUpTypes.SPEED_BOOST:
        newPowerUp = new PowerUp(PowerUpTypes.SPEED_BOOST)
        break
      case PowerUpTypes.SIZE_BOOST:
        newPowerUp = new PowerUp(PowerUpTypes.SIZE_BOOST)
        break
      default:
        return
    }

    Game.powerUps.push(newPowerUp)
    EventHandler.broadcast(ServerEvent.POWER_UP_ADDED, newPowerUp)
  }

  static setPlayerName(id: string, name: string) {
    const player = Game.getPlayer(id)
    player.name = name
  }

  static setPlayerColor(id: string, color: string) {
    const player = Game.getPlayer(id)
    player.color = color
  }

  private static _incrementScore(score: number): number {
    return ++score
  }
}
