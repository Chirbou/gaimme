import { Coords } from '@common/Coords'
import { Size } from '@common/Size'
import { UtilRandom } from '@common/UtilRandom'
import { CommonConfig } from '@common/CommonConfig'
import { PlayerNetwork } from '@common/PlayerNetwork'
import { Vector2D } from '@common/Vector2D'

export class Player implements PlayerNetwork {
  position: Coords
  color: string
  size: Size
  score: number
  velocity: Vector2D
  name: string

  constructor(public id: string) {
    this.size = {
      width: CommonConfig.PLAYER_WIDTH,
      height: CommonConfig.PLAYER_HEIGHT,
    }
    this.position = UtilRandom.randomPosition(this.size)
    this.color = UtilRandom.randomColor()
    this.score = 0
    this.velocity = { x: 0, y: 0 }
    this.name = id
  }
}
