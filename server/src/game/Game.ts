import { UtilRandom } from "@common/UtilRandom"
import { PlayerNetwork } from "@common/PlayerNetwork"
import { Player } from "./Player"
import { Coords } from "@common/Coords"
import { CommonConfig } from "@common/CommonConfig"
import { PowerUp } from "./PowerUp"
import { PowerUpTypes } from "@common/constants/PowerUpTypes"
import { GameConstants } from "src/GameConstants"
import { PowerUpNetwork } from "@common/PowerUpNetwork"

export class Game {
  static players: PlayerNetwork[] = []
  static powerUps: PowerUpNetwork[] = []

  static setup() {
    UtilRandom.init(CommonConfig.GAME_WIDTH - CommonConfig.PLAYER_WIDTH, CommonConfig.GAME_HEIGHT - CommonConfig.PLAYER_HEIGHT);
    Game.initPowerUps(PowerUpTypes.SPEED_BOOST, GameConstants.SPEEDBOOST_COUNT);
    Game.initPowerUps(PowerUpTypes.SIZE_BOOST, GameConstants.SIZEBOOST_COUNT);
  }

  static createPlayer(id: string): Player {
    const player = new Player(id)
    Game.players.push(player)
    return player
  }

  static removePlayer(id: string) {
    Game.players = Game.players.filter(p => p.id !== id)
  }

  static removePowerUp(id: string) {
    Game.powerUps = Game.powerUps.filter(p => p.id !== id)
  }

  static setPosition(id: string, position: Coords) {
    const player = Game.getPlayer(id)
    if (player) {
      player.position = position
    } else {
      console.warn(`[WARN] Player ${id} not found.`)
    }
  }

  static setPlayerData(playerData: PlayerNetwork) {
    const player = Game.getPlayer(playerData.id)
    if (player) {
      player.position = playerData.position
      player.size = playerData.size
      player.velocity = playerData.velocity
    } else {
      console.warn(`[WARN] Player ${playerData.id} not found.`)
    }
  }

  static getPlayer(id: string) {
    return Game.players.find(p => p.id === id)
  }

  static initPowerUps(type, count) {
    for (let i = 1; i <= count; ++i) {
      Game.powerUps.push(new PowerUp(type))
    }
  }
}
