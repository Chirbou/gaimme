import express, { Application } from 'express'
import { Server } from 'http'
import { AppConfig } from './config/AppConfig'
import { EventHandler } from './EventHandler'
import expressStaticGzip from 'express-static-gzip'

export class App {
  app: Application
  server: Server

  constructor() {
    this.app = express()
  }

  run() {
    const port = +process.env.PORT || +AppConfig.PORT
    this.server = this.app.listen(port, () => {
      console.log(
        `Server listening on http://${AppConfig.HOST}:${AppConfig.PORT}`
      )
      EventHandler.init(this.server)
    })

    this.app.use(expressStaticGzip(`${__dirname}/../../client/dist/`, {}))
    this.app.use(`*`, expressStaticGzip(`${__dirname}/../../client/dist/`, {}))
  }
}
