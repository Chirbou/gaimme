import { Socket } from "socket.io";
import { ClientEvent } from "@common/ClientEvent";
import { ServerEvent } from "@common/ServerEvent";
import { UtilParse } from "@common/UtilParse";

export class Client {
  constructor(public id: string, protected socket: Socket) { }

  emit(event: ServerEvent, data: any = null) {
    try {
      const parsedData = UtilParse.stringify(data)
      this.socket.emit(event, parsedData)
    } catch (e) { }
  }

  on(event: ClientEvent, callback: any) {
    this.socket.on(event, callback)
  }
}
