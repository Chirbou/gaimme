import { Size } from './Size'
import { Coords } from './Coords'

export class UtilRandom {
  static maxWidth: number = -1
  static maxHeight: number = -1

  static init(maxWidth: number, maxHeight: number) {
    this.maxWidth = maxWidth
    this.maxHeight = maxHeight
  }

  static randomPosition(size: Size = { width: 0, height: 0 }): Coords {
    return {
      x: Math.floor(Math.random() * (UtilRandom.maxWidth - size.width)),
      y: Math.floor(Math.random() * (UtilRandom.maxHeight - size.height)),
    }
  }

  static randomId() {
    return '_' + Math.random().toString(36).substr(2, 9)
  }

  static randomPower() {
    return 0.5 + Math.random()
  }

  static randomColor(): string {
    return `rgba(${UtilRandom.random255Code()},${UtilRandom.random255Code()},${UtilRandom.random255Code()}, 1)`
  }

  private static random255Code(): number {
    return 100 + Math.floor(Math.random() * 155)
  }

  static rgbaToHex(rgba: string) {
    const inParts = rgba.substring(rgba.indexOf('(')).split(',')
    const r = parseInt(UtilRandom.trim(inParts[0].substring(1)), 10)
    const g = parseInt(UtilRandom.trim(inParts[1]), 10)
    const b = parseInt(UtilRandom.trim(inParts[2]), 10)
    const a = +parseFloat(
      UtilRandom.trim(inParts[3].substring(0, inParts[3].length - 1))
    ).toFixed(2)

    const outParts = [
      r.toString(16),
      g.toString(16),
      b.toString(16),
      Math.round(a * 255)
        .toString(16)
        .substring(0, 2),
    ]

    // Pad single-digit output values
    outParts.forEach((part, i) => {
      if (part.length === 1) {
        outParts[i] = `0${part}`
      }
    })

    return '#' + outParts.join('')
  }

  static trim(str: string) {
    return str.replace(/^\s+|\s+$/gm, '')
  }

  static isLight(color: string): boolean {
    let r: string | number
    let g: string | number
    let b: string | number
    // Check the format of the color, HEX or RGB?
    if (color.match(/^rgb/)) {
      // If HEX --> store the red, green, blue values in separate variables
      const colors: any = color.match(
        /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
      )

      r = colors[1]
      g = colors[2]
      b = colors[3]
    } else {
      // If RGB --> Convert it to HEX: http://gist.github.com/983661
      const colors: any = +(
        '0x' + color.slice(1).replace((color.length < 5 && /./g) as any, '$&$&')
      )

      r = colors >> 16
      g = (colors >> 8) & 255
      b = colors & 255
    }

    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
    const hsp = Math.sqrt(
      0.299 * (+r * +r) + 0.587 * (+g * +g) + 0.114 * (+b * +b)
    )

    // Using the HSP value, determine whether the color is light or dark
    if (hsp > 100) {
      return true
    } else {
      return false
    }
  }
}
