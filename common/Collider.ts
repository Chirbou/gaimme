import { Size } from "./Size";
import { Coords } from "./Coords";

const DEFAULT_SIZE: Size = {
  height: 1,
  width: 1,
}

export class Collider {
  static init() { }

  static areColliding(obj1: Coords, obj2: Coords, sizeObj1: Size = DEFAULT_SIZE, sizeObj2: Size = DEFAULT_SIZE, offset: number = 0) {
    return Collider.computeCollisions(obj1, obj2, sizeObj1, sizeObj2, offset);
  }

  protected static computeCollisions(obj1: Coords, obj2: Coords, sizeObj1: Size = DEFAULT_SIZE, sizeObj2: Size = DEFAULT_SIZE, offset: number = 0): boolean {
    const iw = sizeObj1.width
    const ih = sizeObj1.height
    const jh = sizeObj2.width
    const jw = sizeObj2.height
    const o = offset

    return (obj2.x <= obj1.x + iw && obj2.x + jw >= obj1.x && obj2.y <= obj1.y + ih && obj2.y + jh >= obj1.y)
  }
}
