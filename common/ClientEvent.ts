export enum ClientEvent {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect',
  PLAYER_POSITIONS = 'playerPositions',
  MOVE = 'move',
  SEND_PLAYER_DATA = 'send_player_data',
  POWER_UP_TAKEN = 'power_up_taken',
  BUMP = 'bump',
  SEND_PLAYER_CUSTOM_DATA = 'send_player_custom_data',
}
