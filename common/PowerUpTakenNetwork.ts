import { Coords } from "./Coords";
import { PowerUpTypes } from "./constants/PowerUpTypes";

export interface PowerUpTakenNetwork {
  id: string,
  playerId: string,
  type: PowerUpTypes,
  position: Coords,
}
