export enum ServerEvent {
  SERVER_INIT = 'server_init',
  INIT = 'init',
  SET_PLAYERS = 'set_players',
  EMIT_POWERUPS = 'emit_powerups',
  POWER_UP_ADDED = 'power_up_added',
  POWER_UP_REMOVED = 'power_up_removed',
  SCORES = 'scores',
  PLAYER_JOIN = 'player_join',
  PLAYER_LEAVE = 'player_leave',
}
