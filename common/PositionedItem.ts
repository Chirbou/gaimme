import { Coords } from "./Coords";

export interface PositionedItem { id: string, position: Coords }
