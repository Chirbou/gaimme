import stringify from 'fast-json-stable-stringify'

export class ParseException {
  constructor(public message: string) {
    console.error(message)
  }
}

export class UtilParse {
  static parse(data: string): any {
    try {
      return JSON.parse(data)
    } catch (e) {
      throw new ParseException(`JSON parse error : ${e} with data ${data}`);
    }
  }

  static stringify(data: any): string {
    try {
      return stringify(data)
    } catch (e) {
      throw new ParseException(`JSON stringify error : ${e} with data ${data}`);
    }
  }

  static parseMap(strMap: string): Map<any, any> {
    try {
      return new Map(JSON.parse(strMap))
    } catch (e) {
      throw new ParseException(`JSON parseMap error : ${e} with data ${strMap}`);
    }
  }
}
